This repository contains plots and pictures of the ATLAS magnetic field.

The file 'ATLASBField_rz.root' was created on 30.01.2018 from 'ATLASBField_xyz.root' using the 'InterpolatedBFieldWriter' of 'acts-framework'. It contains a tree of the ATLAS BField in rz-coordinates.

command: ./build/bin/ACTFWBFieldExample --bf-map="../acts-data/MagneticField/ATLAS/ATLASBField_xyz.txt" --bf-file-out="ATLASBField_rz.root" --bf-out-rz=true


The file 'ATLASBField_rz_Map.root' was created from 'ATLASBField_rz.root' using the root script 'printBField' of 'acts-framework':

printBField("ATLASBField_rz.root","bField","ATLASBField_rz_Map.root",0.,10.,-15.,15.,150.)

It contains a histogram of the ATLAS magnetic field map in rz.


The file 'ATLASBField_xyz_Map.root' was created from 'ATLASBField_xyz.root' using the root script 'printBField' of 'acts-framework' :

printBField("../acts-data/MagneticField/ATLAS/ATLASBField_xyz.root","bField","ATLASBField_xyz_Map.root",-10.,10.,-15.,15.,150.)

It contains histograms of the ATLS magnetic field map in xy,zx and zy.


The file 'ATLASBField_ID_rz.root' was created on 30.01.2018 from 'ATLASBField_xyz.root' using the 'InterpolatedBFieldWriter' of 'acts-framework'. It contains a tree of the ATLAS BField if the Inner detector (ID) in rz-coordinates.

command: ./build/bin/ACTFWBFieldExample --bf-map="../acts-data/MagneticField/ATLAS/ATLASBField_xyz.txt" --bf-file-out="ATLASBField_ID_rz.root" --bf-out-rz=true --bf-rRange=0 1100 --bf-zRange=-3300 3300


The file 'ATLASBField_ID_rz_Map.root' was created from 'ATLASBField_ID_rz.root' using the root script 'printBField' of 'acts-framework':

printBField("ATLASBField_ID_rz.root","bField","ATLASBField_ID_rz_Map.root”,0.,1.1,-3.3,3.3,150.)

It contains a histogram of the ATLAS magnetic field map of the inner detector (ID) in rz.


The files 'ATLASBField_rz.png', 'ATLASBField_xy.png', 'ATLASBField_zx.png’, 'ATLASBField_ID_rz.png' and 'ATLASBField_zy.png' are screen shots of the above histograms.


